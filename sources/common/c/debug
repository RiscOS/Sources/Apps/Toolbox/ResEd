/* Copyright 1998 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*-*-C-*-
 *
 * Debugging
 */

#include "resed.h"

#include "swicall.h"


#if DEBUG

#include <stdarg.h>

#define HostFS_HostVdu 0x40100
#define HostFS_TubeVdu 0x40101

static FILE *f = NULL;


void debug_file (char *filename)
{
    if (f)
        fclose (f);
    if (filename)
        f = fopen (filename, "w");
}


int dprintf (char *format, ...)
{
    int ret = 0;
    va_list list;
    Bool doit = TRUE;
    va_start (list, format);
    if (!f)
        doit = swi (HostFS_HostVdu, END) == NULL;
    if (doit)
        ret = vfprintf(f ? f : stdout, format, list);
    if (!f)
        swi (HostFS_TubeVdu, END);
    va_end(list);
    if (f)
        fflush(f);
    return ret;
}


void display (char *format, ...)

  /* like printf, but displays the result in a wimp "error" box */

{
    char s[256];
    va_list ap;
    error e;

    va_start (ap, format);
    vsprintf (s, format, ap);
    va_end (ap);

    e.errnum = 0;
    strcpy (e.errmess, s);

    swi ( Wimp_ReportError,
          R0, (int)&e,
          R1, 1+16,          /* just an OK button, omit "Error from" */
          R2, "Debug",
          END );

    return;
}


#endif
